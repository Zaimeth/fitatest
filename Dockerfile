FROM node:alpine3.14
# Please continue here

# Date
RUN apk update && \
    apk add --no-cache tzdata

ENV TZ=Asia/Jakarta
#Directory
WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install 
RUN npm install -g sequelize-cli
RUN sequelize init

COPY . .
RUN rm -rf /usr/src/app/config/config.json
COPY default.config.js /usr/src/app/config/config.js

EXPOSE 3000

RUN chmod +x entrypoint.sh      
ENTRYPOINT ["sh","./entrypoint.sh"]

CMD [ "npm" , "start" ]
